const products = [
    { id: "35af6924-9a6f-4512-9453-65df66226652", name: 'Bread', description: 'Baguette from France', price: 200 },
    { id: "bb433d88-bfec-4897-8321-733535a2541d", name: 'Beer', description: 'Belgium Beer, the best one', price: 400 },
    { id: "2cb84e5c-4065-48d7-b6fe-a5c25354bf28", name: 'Carrot', description: 'Dutch Carrot, very orange', price: 150 }
]

module.exports = products