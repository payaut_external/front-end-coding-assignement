// Config
const express = require('express')
const { v4: uuidv4 } = require('uuid');
const app = express()
const port = 3000

app.use(express.json())

// Data

const products = require("./data.js")

// Endpoints

/**
 * GET /api/products
 * 
 * Return the list of products with status code 200.
 */

app.get('/api/products', (req, res) => {
    res.json(products)
})

/**
 * GET /api/products/:productID
 * 
 * productID : String
 * 
 * Return the product for the given id.
 * 
 * If found return status code 200 and the resource.
 * If not found return status code 404.
 */

app.get('/api/products/:productID', (req, res) => {
    const id = String(req.params.productID)
    const product = products.find(product => product.id === id)

    if (!product) {
        return res.status(404).send('Product not found')
    }

    res.json(product)
})

/**
 * POST /api/products/:productID
 * 
 * productID : String
 * 
 * Add a new product to the current list.
 * 
 * Return status code 201.
 */

app.post('/api/products', (req, res) => {
    const newProduct = {
        id: uuidv4(),
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    products.push(newProduct)

    res.status(201).json(newProduct)
})

/**
 * PUT /api/products/:productID
 * 
 * productID : String
 * 
 * Update the target product for the given id.
 * 
 * If updated return status code 200 and the resource.
 * If not found return status code 404.
 */

app.put('/api/products/:productID', (req, res) => {
    const id = String(req.params.productID)
    const index = products.findIndex(product => product.id === id)

    if (index === -1) {
        return res.status(404).send('Product not found')
    }

    const updatedProduct = {
        id: products[index].id,
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    products[index] = updatedProduct

    res.status(200).json(updatedProduct)
})

/**
 * DELETE /api/products/:productID
 * 
 * productID : String
 * 
 * Delete the target product for the given id.
 * 
 * If deleted return status code 200.
 * If not found return status code 404.
 */

app.delete('/api/products/:productID', (req, res) => {
    const id = String(req.params.productID)
    const index = products.findIndex(product => product.id === id)

    if (index === -1) {
        return res.status(404).send('Product not found')
    }

    products.splice(index,1)

    res.status(200).json('Product deleted')
})

// Run
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})