# Front-End Coding Assignement

Front-End Coding Assignement for the interview process.

## Subject
A grocery store wants to create a new website to sell its products online. To do this, they ask you to set up a simple front-end application that would show them how :

- Customers can buy products on this new platform
- Admins can manage all displayed products

The expected result of this request is to have a simple proof of concept where we can validate these 2 scenarios with certain requirements.
### Business rules

As this is a proof of concept, there is no need to have separate spaces for different users (customer and admin) -> Everything can be in the same page.

Here are the business requirements:

- All possible products are displayed
- Users must be able to add/remove products to their cart
- Cart :
    - Can have many different items
    - Must not contain more than 20 items in total
    - Should automatically refresh and display the newly calculated price
- Users who have a total price equal to or greater than €100, will receive a 5% discount on the cart
- From the same page, users should be able to add/edit/delete product in the store.
- Each product :
    - Has a title, description and a price (required)
    - Has a positive price displayed in Euros (*Ex: 15.45*)

### Setup

The server-side is supplied, so you don’t have to create one yourself.
The functionality of the server is very limited and there is room for improvement - For instance, there is no check on the given payload.

#### Steps
```sh
git clone https://gitlab.com/payaut_external/front-end-coding-assignement.git
cd front-end-coding-assignement/server
npm i
node index.js
```

Here are the all calls available (can also look at their implementation inside `server/index.js`).

Product object :
```json
{
    "id": "35af6924-9a6f-4512-9453-65df66226652",
    "name": "Bread",
    "description": "Baguette from France",
    "price": 200
}
```

List all current products :
```sh
curl --location --request GET 'localhost:3000/api/products'
```

Get a specific product :
```sh
curl --location --request GET 'localhost:3000/api/products/35af6924-9a6f-4512-9453-65df66226652'
```

Create a new product :
```sh
curl --location --request POST 'localhost:3000/api/products' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Water",
    "description": "River from the Canal",
    "price": 100
}'
```

Update a specific product :
```sh
curl --location --request PUT 'localhost:3000/api/products/35af6924-9a6f-4512-9453-65df66226652' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Water",
    "description": "Finally the water is quite good",
    "price": 150
}'
```

Delete a specific product :
```sh
curl --location --request DELETE 'localhost:3000/api/products/35af6924-9a6f-4512-9453-65df66226652'
```

## Instructions

- Write a front-end application that satisties all requirements
- You are free to use any front-end framework/library
- You may assume your users will always be using a desktop
- You have to make sure the code compiles and contains unit tests
- Keep it simple

### Expectations

- Business rules correctness
- Code readability
- Quality of your unit tests
- A simple UI/UX that is comfortable to use

### Notes

- You’re free to ask questions if anything is not clear
- There are no bonus points for making it bigger than needed !

### Solution
Please create your own repository based on a checkout of this project. Do not fork / branch this project when creating your solution as it will be visible to other applicants. Once your code is ready, please send us the link to your repository and we will review it.

*We will ask you to make that project private as soon as the interview process is over.*